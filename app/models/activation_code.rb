class ActivationCode < ActiveRecord::Base
  belongs_to :book_info
  # validates_associated :book_info
  validates :book_info , presence: true

  validates :serial_number, length: { is: 6 },uniqueness: true
  validates :code, presence: true,uniqueness: true
  validates :has_use, presence: true
	
end
