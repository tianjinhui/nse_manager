class BookInfo < ActiveRecord::Base
  has_many :activation_codes

  validates :name, presence: true,uniqueness: true
end
