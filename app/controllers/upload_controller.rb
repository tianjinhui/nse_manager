require 'csv'

class UploadController < ApplicationController

  def index
    @book_infos = BookInfo.all
  end

  # 将scv中的数据保存到数据库中，并关联用户选择的册名
  def import
    select_book_info_id = params[:select_book_info_id]

    uploaded_file = params[:file]
    uploaded_file_content = uploaded_file.read

    #保存文件到本地,文件名为原始文件名加上时间戳
    f = File.new("import/#{uploaded_file.original_filename}_#{Time.now.strftime("%Y%m%d%H%M%S") }",'w') 
    f  << uploaded_file_content
    f.close()


    code_list = [] #保存临时的序列号列表，等待下一步验证
    @error_list = [] # 保存所有的

    # 将文件交给csv模块进行解析
    CSV.parse(uploaded_file_content) do |serial_number,code|
       one_code = ActivationCode.new()
       one_code.serial_number = serial_number
       one_code.code = code
       one_code.has_use = false
       one_code.book_info_id = select_book_info_id
       code_list << one_code

       if(one_code.invalid?)
         @error_list << '行:: ' + serial_number.to_s
         @error_list << one_code.errors.full_messages
       end

    end

    if(@error_list.length == 0)
      code_list.each do |one_code|
        one_code.save()
      end
      redirect_to '/activation_codes'
    else
      render :index
    end

  end

end