class ActivationCodesController < ApplicationController
  before_action :set_activation_code, only: [:show, :edit, :update, :destroy]

  # GET /activation_codes
  # GET /activation_codes.json
  def index
    @activation_codes = ActivationCode.all
  end

  # GET /activation_codes/1
  # GET /activation_codes/1.json
  def show
  end

  # GET /activation_codes/new
  def new
    @activation_code = ActivationCode.new
  end

  # GET /activation_codes/1/edit
  def edit
  end

  # POST /activation_codes
  # POST /activation_codes.json
  def create
    @activation_code = ActivationCode.new(activation_code_params)

    respond_to do |format|
      if @activation_code.save
        format.html { redirect_to @activation_code, notice: 'Activation code was successfully created.' }
        format.json { render :show, status: :created, location: @activation_code }
      else
        format.html { render :new }
        format.json { render json: @activation_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /activation_codes/1
  # PATCH/PUT /activation_codes/1.json
  def update
    respond_to do |format|
      if @activation_code.update(activation_code_params)
        format.html { redirect_to @activation_code, notice: 'Activation code was successfully updated.' }
        format.json { render :show, status: :ok, location: @activation_code }
      else
        format.html { render :edit }
        format.json { render json: @activation_code.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /activation_codes/1
  # DELETE /activation_codes/1.json
  def destroy
    @activation_code.destroy
    respond_to do |format|
      format.html { redirect_to activation_codes_url, notice: 'Activation code was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_activation_code
      @activation_code = ActivationCode.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def activation_code_params
      params.require(:activation_code).permit(:serial_number, :code, :has_use, :remark, :book_info_id)
    end
end
