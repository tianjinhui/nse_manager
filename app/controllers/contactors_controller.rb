class ContactorsController < ApplicationController
  before_action :set_contactor, only: [:show, :edit, :update, :destroy]

  # GET /contactors
  # GET /contactors.json
  def index
    @contactors = Contactor.all
  end

  # GET /contactors/1
  # GET /contactors/1.json
  def show
  end

  # GET /contactors/new
  def new
    @contactor = Contactor.new
  end

  # GET /contactors/1/edit
  def edit
  end

  # POST /contactors
  # POST /contactors.json
  def create
    @contactor = Contactor.new(contactor_params)

    respond_to do |format|
      if @contactor.save
        format.html { redirect_to @contactor, notice: 'Contactor was successfully created.' }
        format.json { render :show, status: :created, location: @contactor }
      else
        format.html { render :new }
        format.json { render json: @contactor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contactors/1
  # PATCH/PUT /contactors/1.json
  def update
    respond_to do |format|
      if @contactor.update(contactor_params)
        format.html { redirect_to @contactor, notice: 'Contactor was successfully updated.' }
        format.json { render :show, status: :ok, location: @contactor }
      else
        format.html { render :edit }
        format.json { render json: @contactor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contactors/1
  # DELETE /contactors/1.json
  def destroy
    @contactor.destroy
    respond_to do |format|
      format.html { redirect_to contactors_url, notice: 'Contactor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contactor
      @contactor = Contactor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contactor_params
      params.require(:contactor).permit(:name, :email, :phone)
    end
end
