class ContactContentsController < ApplicationController
  before_action :set_contact_content, only: [:show, :edit, :update, :destroy]

  # GET /contact_contents
  # GET /contact_contents.json
  def index
    @contact_contents = ContactContent.all
  end

  # GET /contact_contents/1
  # GET /contact_contents/1.json
  def show
  end

  # GET /contact_contents/new
  def new
    @contact_content = ContactContent.new

    if(params[:contactor_id] != nil)
      @contact_content.contactor = Contactor.find(params[:contactor_id])
    end
  end

  # GET /contact_contents/1/edit
  def edit
  end

  # POST /contact_contents
  # POST /contact_contents.json
  def create
    @contact_content = ContactContent.new(contact_content_params)

    respond_to do |format|
      if @contact_content.save
        format.html { redirect_to @contact_content.contactor, notice: 'Contact content was successfully created.' }
        format.json { render :show, status: :created, location: @contact_content }
      else
        format.html { render :new }
        format.json { render json: @contact_content.errors, status: :unprocessable_entity }
      end
    end


  end

  # PATCH/PUT /contact_contents/1
  # PATCH/PUT /contact_contents/1.json
  def update
    respond_to do |format|
      if @contact_content.update(contact_content_params)
        format.html { redirect_to @contact_content, notice: 'Contact content was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact_content }
      else
        format.html { render :edit }
        format.json { render json: @contact_content.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contact_contents/1
  # DELETE /contact_contents/1.json
  def destroy
    @contact_content.destroy
    respond_to do |format|
      format.html { redirect_to contact_contents_url, notice: 'Contact content was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact_content
      @contact_content = ContactContent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_content_params
      params.require(:contact_content).permit(:content_type, :content_desc, :solution, :contactor_id)
    end
end
