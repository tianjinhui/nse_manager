json.array!(@activation_codes) do |activation_code|
  json.extract! activation_code, :id, :serial_number, :code, :has_use, :remark, :book_info_id
  json.url activation_code_url(activation_code, format: :json)
end
