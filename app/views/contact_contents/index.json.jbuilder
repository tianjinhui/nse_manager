json.array!(@contact_contents) do |contact_content|
  json.extract! contact_content, :id, :content_type, :content_desc, :solution, :contactor_id
  json.url contact_content_url(contact_content, format: :json)
end
