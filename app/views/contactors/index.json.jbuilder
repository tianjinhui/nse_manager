json.array!(@contactors) do |contactor|
  json.extract! contactor, :id, :name, :email, :phone
  json.url contactor_url(contactor, format: :json)
end
