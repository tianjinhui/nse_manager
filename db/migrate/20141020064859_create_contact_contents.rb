class CreateContactContents < ActiveRecord::Migration
  def change
    create_table :contact_contents do |t|
      t.string :content_type
      t.text :content_desc
      t.text :solution
      t.references :contactor, index: true

      t.timestamps
    end
  end
end
