class CreateActivationCodes < ActiveRecord::Migration
  def change
    create_table :activation_codes do |t|
      t.string :serial_number
      t.string :code
      t.boolean :has_use
      t.text :remark
      t.references :book_info

      t.timestamps
    end
  end
end
