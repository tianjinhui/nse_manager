class CreateBookInfos < ActiveRecord::Migration
  def change
    create_table :book_infos do |t|
      t.string :name

      t.timestamps
    end
  end
end
