require 'test_helper'

class ContactorsControllerTest < ActionController::TestCase
  setup do
    @contactor = contactors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contactors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contactor" do
    assert_difference('Contactor.count') do
      post :create, contactor: { email: @contactor.email, name: @contactor.name, phone: @contactor.phone }
    end

    assert_redirected_to contactor_path(assigns(:contactor))
  end

  test "should show contactor" do
    get :show, id: @contactor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @contactor
    assert_response :success
  end

  test "should update contactor" do
    patch :update, id: @contactor, contactor: { email: @contactor.email, name: @contactor.name, phone: @contactor.phone }
    assert_redirected_to contactor_path(assigns(:contactor))
  end

  test "should destroy contactor" do
    assert_difference('Contactor.count', -1) do
      delete :destroy, id: @contactor
    end

    assert_redirected_to contactors_path
  end
end
