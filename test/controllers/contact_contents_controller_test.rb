require 'test_helper'

class ContactContentsControllerTest < ActionController::TestCase
  setup do
    @contact_content = contact_contents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contact_contents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contact_content" do
    assert_difference('ContactContent.count') do
      post :create, contact_content: { contactor_id: @contact_content.contactor_id, content_desc: @contact_content.content_desc, content_type: @contact_content.content_type, solution: @contact_content.solution }
    end

    assert_redirected_to contact_content_path(assigns(:contact_content))
  end

  test "should show contact_content" do
    get :show, id: @contact_content
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @contact_content
    assert_response :success
  end

  test "should update contact_content" do
    patch :update, id: @contact_content, contact_content: { contactor_id: @contact_content.contactor_id, content_desc: @contact_content.content_desc, content_type: @contact_content.content_type, solution: @contact_content.solution }
    assert_redirected_to contact_content_path(assigns(:contact_content))
  end

  test "should destroy contact_content" do
    assert_difference('ContactContent.count', -1) do
      delete :destroy, id: @contact_content
    end

    assert_redirected_to contact_contents_path
  end
end
