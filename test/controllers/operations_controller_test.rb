require 'test_helper'

class OperationsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get flow" do
    get :flow
    assert_response :success
  end

end
