require 'test_helper'

class ActivationCodesControllerTest < ActionController::TestCase
  setup do
    @activation_code = activation_codes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:activation_codes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create activation_code" do
    assert_difference('ActivationCode.count') do
      post :create, activation_code: { book_info_id: @activation_code.book_info_id, code: @activation_code.code, has_use: @activation_code.has_use, remark: @activation_code.remark, serial_number: @activation_code.serial_number }
    end

    assert_redirected_to activation_code_path(assigns(:activation_code))
  end

  test "should show activation_code" do
    get :show, id: @activation_code
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @activation_code
    assert_response :success
  end

  test "should update activation_code" do
    patch :update, id: @activation_code, activation_code: { book_info_id: @activation_code.book_info_id, code: @activation_code.code, has_use: @activation_code.has_use, remark: @activation_code.remark, serial_number: @activation_code.serial_number }
    assert_redirected_to activation_code_path(assigns(:activation_code))
  end

  test "should destroy activation_code" do
    assert_difference('ActivationCode.count', -1) do
      delete :destroy, id: @activation_code
    end

    assert_redirected_to activation_codes_path
  end
end
