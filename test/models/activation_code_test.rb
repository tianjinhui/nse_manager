require 'test_helper'

class ActivationCodeTest < ActiveSupport::TestCase
  test "serial_number length is 6" do
    ac = ActivationCode.new()
    assert_not ac.valid?

    ac.serial_number = '0ASDF'
    assert_not ac.valid?

    ac.serial_number = 'QWERTYU'
    assert_not ac.valid?

     ac.serial_number = 'QWERTY'
     assert ac.valid?
  end
end
