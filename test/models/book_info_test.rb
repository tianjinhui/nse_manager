require 'test_helper'

class BookInfoTest < ActiveSupport::TestCase
  test "book_info must name" do
    bi = BookInfo.new()
    assert_not  bi.valid?

    bi.name = "test name"
    assert bi.valid?
  end
end
